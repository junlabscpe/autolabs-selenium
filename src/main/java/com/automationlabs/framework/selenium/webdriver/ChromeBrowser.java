package com.automationlabs.framework.selenium.webdriver;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeBrowser extends Browser{

    public ChromeBrowser(String chromeDrivePath) {
        System.setProperty("webdriver.chrome.driver", chromeDrivePath);
        driver = new ChromeDriver();
    }
    
}
