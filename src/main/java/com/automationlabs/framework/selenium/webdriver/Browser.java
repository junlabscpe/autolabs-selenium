package com.automationlabs.framework.selenium.webdriver;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class Browser {

    protected WebDriver driver;

    public void click(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    public void navigate(String url) {
        driver.get(url);
    }

    public void sendKeys(String xpath, String text) {
        driver.findElement(By.xpath(xpath)).sendKeys(text);
    }

    public String getText(String xpath) {
        return driver.findElement(By.xpath(xpath)).getText();
    }

    public boolean isElementPresent(String xpath) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));
        return elements.size() > 0;
    }

    public void wait(int seconds) throws Exception {
        Thread.sleep(seconds * 1000);
    }

    public void close() {
        driver.quit();
    }

}
