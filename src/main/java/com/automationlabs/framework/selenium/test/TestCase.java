package com.automationlabs.framework.selenium.test;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.automationlabs.framework.selenium.datadriven.Step;
import com.automationlabs.framework.selenium.keyworddriven.Browser;
import com.automationlabs.framework.selenium.keyworddriven.Page;
import com.automationlabs.framework.selenium.utility.ExcelReader;

public class TestCase {

	private static final Logger log = Logger.getLogger(TestCase.class);
	private WebDriver driver;

	private String testScript;
	private static final String OPEN_BROWSER_METHOD_NAME = "openBrowser";

	@BeforeTest(alwaysRun = true)
	public void setTest(ITestContext context){
		testScript = context.getCurrentXmlTest().getParameter("file");
	}

	@DataProvider(name = "test case")
	public Object[][] readTestCase(ITestContext context) throws IOException{
		List<Step> steps = ExcelReader.getSteps(testScript, "Sheet1");
		log.info("Reading : " + testScript);
		Object[][] res = new Object[steps.size()][1];
		for(int i = 0; i < steps.size(); i++){
			Step step = steps.get(i);
			step.setContext(context);
			res[i][0] = step;
		}
		return res;
	}

	@Test(dataProvider = "test case")
	public void steps(Step step){
		log.info(step);
		SoftAssert soft = new SoftAssert();
		if(step.getKeyword().equals(OPEN_BROWSER_METHOD_NAME)){
			driver = Browser.openBrowser(step, soft);
		}
		else{
			Method method;
			try{
				method = Browser.class.getMethod(step.getKeyword(), WebDriver.class, Step.class, SoftAssert.class);
				method.invoke(Browser.class, driver, step, soft);
			}catch(NoSuchMethodException nsme){
				try {
					method = Page.class.getMethod(step.getKeyword(), WebDriver.class, Step.class, SoftAssert.class);
					method.invoke(Page.class, driver, step, soft);
				} catch (Exception e) {
					soft.assertTrue(false, e.getMessage() + ", Keyword not found.");
					log.error(e);
				}
			}catch(Exception e){
				soft.assertTrue(false, e.getMessage() + ", Keyword not found.");
				log.error(e);
			}
		}
		soft.assertAll();
	}

}
