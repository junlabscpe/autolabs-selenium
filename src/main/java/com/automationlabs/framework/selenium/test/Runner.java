package com.automationlabs.framework.selenium.test;

import java.util.ArrayList;
import java.util.List;

import org.testng.ITestNGListener;
import org.testng.TestNG;

public class Runner {

	private Runner(){}

	public static void main(String[] args) {
		List<Class<? extends ITestNGListener>> listeners = new ArrayList<Class<? extends ITestNGListener>>();
		TestNG testng = new TestNG();

		listeners.add(com.automationlabs.framework.selenium.report.HTMLReporterGen.class);

		testng.setXmlSuites(Suite.createSuites());
		testng.setUseDefaultListeners(false);
		testng.setListenerClasses(listeners);

		testng.run();
	}

}
