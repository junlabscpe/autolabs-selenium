package com.automationlabs.framework.selenium.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.automationlabs.framework.selenium.utility.ConfigProperties;
import com.automationlabs.framework.selenium.utility.ConfigPropertyKeys;

public class Suite {

	private Suite(){}

	public static List<XmlSuite> createSuites() {
		File[] suitesFolder = new File(ConfigProperties.getStringProperty(ConfigPropertyKeys.SUITES_LOCATION)).listFiles();
		List<XmlSuite> suites = new ArrayList<XmlSuite>();

		for(int suitesIndex = 0; suitesIndex < suitesFolder.length; suitesIndex++){
			File[] files = new File(suitesFolder[suitesIndex].getAbsolutePath()).listFiles();
			XmlSuite suite = new XmlSuite();

			suite.setName(suitesFolder[suitesIndex].getName());

			for (int i = 0; i < files.length; i++) {
				XmlClass xc = new XmlClass(TestCase.class);
				List<XmlClass> xmlClasses = new ArrayList<XmlClass>();
				xmlClasses.add(xc);
				XmlTest test = new XmlTest();
				test.setName("TEST " + files[i].getName());
				test.setClasses(xmlClasses);
				test.addParameter("file", files[i].getAbsolutePath());

				suite.addTest(test);
				test.setSuite(suite);
			}

			suites.add(suite);
		}

		return suites;
	}

}
