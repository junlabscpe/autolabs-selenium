package com.automationlabs.framework.selenium.keyworddriven;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import org.testng.log4testng.Logger;

import com.automationlabs.framework.selenium.datadriven.Step;
import com.automationlabs.framework.selenium.utility.ConfigProperties;
import com.automationlabs.framework.selenium.utility.ConfigPropertyKeys;
import com.automationlabs.framework.selenium.utility.SeleniumUtils;

import org.apache.commons.io.FileUtils;

public class Page {

	private static final Logger log = Logger.getLogger(Page.class);

	private Page(){}

	/**
	 * This method is only used for framework test. To minimized test duration and generate result in short time.
	 * @param driver
	 * @param step
	 * @param soft
	 */
	public static void dummyMethod(WebDriver driver, Step step, SoftAssert soft){
		try{
			soft.assertEquals(step.getLocator(), step.getTestData());
		}catch(Exception e){
			soft.assertTrue(false,e.getMessage());
			log.error(e.getMessage(),e);
		}
	}

	public static void click(WebDriver driver, Step step, SoftAssert soft){
		try{
			SeleniumUtils.getElement(driver, step).click();
			log.info("Click " + step.getLocator());
			soft.assertTrue(true);
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void enterText(WebDriver driver, Step step, SoftAssert soft){
		try{
			SeleniumUtils.getElement(driver, step).sendKeys(step.getTestData());
			log.info("Enter text " + step.getLocator());
			soft.assertTrue(true);
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void selectByValue(WebDriver driver, Step step, SoftAssert soft){
		try{
			Select select = new Select(SeleniumUtils.getElement(driver, step));
			select.selectByValue(step.getTestData());
			log.info("Select by value from " + step.getLocator());
			soft.assertTrue(true);
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void selectByIndex(WebDriver driver, Step step, SoftAssert soft){
		try{
			Select select = new Select(SeleniumUtils.getElement(driver, step));
			select.selectByIndex(Integer.parseInt(step.getTestData()));
			log.info("Select by index from " + step.getLocator());
			soft.assertTrue(true);
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void selectByVisibleText(WebDriver driver, Step step, SoftAssert soft){
		try{
			Select select = new Select(SeleniumUtils.getElement(driver, step));
			select.selectByVisibleText(step.getTestData());
			log.info("Select by visible text from " + step.getLocator());
			soft.assertTrue(true);
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void assertEquals(WebDriver driver, Step step, SoftAssert soft){
		try{
			soft.assertEquals(SeleniumUtils.getElement(driver, step).getText(), step.getTestData());
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void assertContains(WebDriver driver, Step step, SoftAssert soft){
		try{
			soft.assertTrue(SeleniumUtils.getElement(driver, step).getText().contains(step.getTestData()));
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void isDisplayed(WebDriver driver, Step step, SoftAssert soft){
		try{
			boolean isVisible = driver.findElement(SeleniumUtils.getLocator(step.getLocatorType(),step.getLocator())).isDisplayed();
			soft.assertEquals(isVisible, Boolean.parseBoolean(step.getTestData()), "Is element displayed?");
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void isDisplayedNoWait(WebDriver driver, Step step, SoftAssert soft){
		SeleniumUtils.turnDefaultImplicitWaitOff(driver);
		isDisplayed(driver, step, soft);
		SeleniumUtils.turnDefaultImplicitWaitOn(driver);
	}

	public static void isEnabled(WebDriver driver, Step step, SoftAssert soft){
		try{
			boolean isEnabled = driver.findElement(SeleniumUtils.getLocator(step.getLocatorType(),step.getLocator())).isEnabled();
			soft.assertEquals(isEnabled, Boolean.parseBoolean(step.getTestData()), "Is element enabled?");
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void isEnabledNoWait(WebDriver driver, Step step, SoftAssert soft){
		SeleniumUtils.turnDefaultImplicitWaitOff(driver);
		isEnabled(driver, step, soft);
		SeleniumUtils.turnDefaultImplicitWaitOn(driver);
	}

	public static void isSelected(WebDriver driver, Step step, SoftAssert soft){
		try{
			boolean isSelected = driver.findElement(SeleniumUtils.getLocator(step.getLocatorType(),step.getLocator())).isSelected();
			soft.assertEquals(isSelected, Boolean.parseBoolean(step.getTestData()), "Is element selected?");
		}catch(Exception e){
			soft.assertTrue(false, e.getMessage());
			log.error(e);
		}
	}

	public static void isSelectedNoWait(WebDriver driver, Step step, SoftAssert soft){
		SeleniumUtils.turnDefaultImplicitWaitOff(driver);
		isSelected(driver, step, soft);
		SeleniumUtils.turnDefaultImplicitWaitOn(driver);
	}

	public static void waitForPresenceOf(WebDriver driver, Step step, SoftAssert soft){
		try{
			long timeout = ((Integer.parseInt(step.getTestData())>0)?
					Long.parseLong(step.getTestData()):
					ConfigProperties.getLongProperty(ConfigPropertyKeys.DEFAULT_WAIT_SEC));
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.presenceOfElementLocated(SeleniumUtils.getLocator(step.getLocatorType(), step.getLocator())));
			soft.assertTrue(true,"Wait for presence of element.");
		}catch(TimeoutException te){
			soft.assertTrue(false,te.getMessage());
			log.error(te.getMessage(), te);
		}catch(Exception e){
			soft.assertTrue(false,e.getMessage());
			log.error(e.getMessage(), e);
		}
	}

	public static void waitForVisibilityOf(WebDriver driver, Step step, SoftAssert soft){
		try{
			long timeout = ((Integer.parseInt(step.getTestData())>0)?
					Long.parseLong(step.getTestData()):
					ConfigProperties.getLongProperty(ConfigPropertyKeys.DEFAULT_WAIT_SEC));
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(SeleniumUtils.getLocator(step.getLocatorType(), step.getLocator())));
			soft.assertTrue(true,"Wait for visibility of element.");
		}catch(TimeoutException te){
			soft.assertTrue(false,te.getMessage());
			log.error(te.getMessage(), te);
		}catch(Exception e){
			soft.assertTrue(false,e.getMessage());
			log.error(e.getMessage(), e);
		}
	}

	public static void sendKeys(WebDriver driver, Step step, SoftAssert soft){
		try{
			driver.findElement(SeleniumUtils.getLocator(step.getLocatorType(), step.getLocator())).sendKeys(Keys.valueOf(step.getTestData()));
		}catch(Exception e){
			soft.assertTrue(false,e.getMessage());
			log.error(e.getMessage(),e);
		}
	}

	public static void captureScreenshot(WebDriver driver, Step step, SoftAssert soft){
        try {
        	String path = step.getContext().getOutputDirectory() + File.separator +
        			step.getContext().getCurrentXmlTest().getName().replace(".xlsx", "");
        	TakesScreenshot capture = (TakesScreenshot) driver;
        	File screenshot = capture.getScreenshotAs(OutputType.FILE);
        	File file = new File(path + File.separator + "Step " + step.getStepNo() + ".png");
        	file.getParentFile().mkdirs();
			FileUtils.copyFile(screenshot, file);
		} catch (IOException e) {
			soft.assertTrue(false,e.getMessage());
			log.error(e.getMessage(),e);
		} catch (Exception e){
			soft.assertTrue(false,e.getMessage());
			log.error(e.getMessage(),e);
		}
	}

}
