package com.automationlabs.framework.selenium.keyworddriven;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.testng.asserts.SoftAssert;

import com.automationlabs.framework.selenium.datadriven.Step;
import com.automationlabs.framework.selenium.utility.ConfigProperties;
import com.automationlabs.framework.selenium.utility.ConfigPropertyKeys;
import com.automationlabs.framework.selenium.utility.SeleniumUtils;

public class Browser {

	private static final Logger log = Logger.getLogger(Browser.class);

	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";
	private static final String OPERA_DRIVER_PROPERTY = "webdriver.opera.driver";

	private Browser(){}

	public static WebDriver openBrowser(Step step, SoftAssert soft){
		WebDriver driver = null;
		try{
			switch(SeleniumUtils.BrowserType.valueOf(step.getTestData().toUpperCase())){
				case CHROME:
					System.setProperty(CHROME_DRIVER_PROPERTY, ConfigProperties.getStringProperty(ConfigPropertyKeys.WEBDRIVER_CHROME));
					driver = new ChromeDriver();
					log.info("Opening Chrome Browser");
					break;
				case FIREFOX:
					System.setProperty(FIREFOX_DRIVER_PROPERTY, ConfigProperties.getStringProperty(ConfigPropertyKeys.WEBDRIVER_FIREFOX));
					driver = new FirefoxDriver();
					log.info("Opening Firefox Browser");
					break;
				case OPERA:
					System.setProperty(OPERA_DRIVER_PROPERTY, ConfigProperties.getStringProperty(ConfigPropertyKeys.WEBDRIVER_OPERA));
					OperaOptions option = new OperaOptions();
					option.setBinary(new File(ConfigProperties.getStringProperty(ConfigPropertyKeys.OPERA_BINARY_LOCAL)));
//					DesiredCapabilities operaCap = DesiredCapabilities.operaBlink();
//					Map<String, Object> map = new HashMap<>();
//					map.put("args", new ArrayList<>());
//					map.put("extensions", new ArrayList<>());
//					map.put("binary", ConfigProperties.getStringProperty(ConfigPropertyKeys.OPERA_BINARY_GRID));
//					operaCap.setCapability("operaOptions", map);
					driver = new OperaDriver(option);
					log.info("Opening Opera Browser");
					break;
				default:
					log.info("Undefined browser name " + step.getTestData());
			}
			if(driver != null){
				driver.manage().timeouts().implicitlyWait(ConfigProperties.getLongProperty(ConfigPropertyKeys.DEFAULT_WAIT_SEC), TimeUnit.SECONDS);
			}
			soft.assertTrue(true);
		}catch(Exception e){
			soft.assertTrue(false,e.getMessage());
			log.error(e);
		}
		return driver;
	}

	public static void closeBrowser(WebDriver driver, Step step, SoftAssert soft){
		try{
			try{
				driver.quit();
			}catch(Exception e){
				log.warn(e.getMessage());
				driver.close();
			}
			log.info("Closing browser...");
			soft.assertTrue(true);
		}catch(Exception e){
			soft.assertTrue(false,e.getMessage());
			log.error(e);
		}
	}

	public static void navigate(WebDriver driver, Step step, SoftAssert soft){
		try{
			driver.get(step.getTestData());
			log.info("Navigating to " + step.getTestData());
			soft.assertTrue(true);
		}catch(Exception e){
			soft.assertTrue(false,e.getMessage());
			log.error(e);
		}
	}

//	private static RemoteWebDriver createRemoteDriver(DesiredCapabilities cap) throws MalformedURLException {
//		String platform = (System.getProperty("gridPlatform") == null)
//				? ConfigurationProperties.getStringProperty(ConfigPropertyKeys.DRIVERS_GRID_PLATFORM)
//				: System.getProperty("gridPlatform");
//		cap.setCapability("platform", platform);
//		cap.setAcceptInsecureCerts(true);
//		log.info("Remote Driver Capabilities : \n" + cap.toString());
//		URL url = new URL(ConfigurationProperties.getStringProperty(ConfigPropertyKeys.DRIVERS_GRID_HUB));
//		log.info("Remote Grid Hub : " + url.toString());
//		return new RemoteWebDriver(url, cap);
//	}

}
