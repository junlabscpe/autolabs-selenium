package com.automationlabs.framework.selenium.datadriven;

import org.testng.ITestContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Step {

	private int stepNo;
	private String keyword;
	private String locatorType;
	private String locator;
	private String testData;
	private transient ITestContext context;

	public int getStepNo() {
		return stepNo;
	}

	public void setStepNo(int stepNo) {
		this.stepNo = stepNo;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getLocator() {
		return locator;
	}

	public void setLocator(String locator) {
		this.locator = locator;
	}

	public String getTestData() {
		return testData;
	}

	public void setTestData(String testData) {
		this.testData = testData;
	}

	public String getLocatorType() {
		return locatorType;
	}

	public void setLocatorType(String locatorType) {
		this.locatorType = locatorType;
	}

	public ITestContext getContext() {
		return context;
	}

	public void setContext(ITestContext context) {
		this.context = context;
	}

	@Override
	public String toString(){
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		return gson.toJson(this);
	}

}
