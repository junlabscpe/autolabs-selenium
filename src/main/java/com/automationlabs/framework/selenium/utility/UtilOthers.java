package com.automationlabs.framework.selenium.utility;

import com.automationlabs.framework.selenium.datadriven.Step;
import com.google.gson.Gson;

public class UtilOthers {

	public static String objectToJson(Object obj){
		Gson gson = new Gson();
		return gson.toJson(obj);
	}

	public static <T> T jsonToObject(String json, Class<T> clazz){
		Gson gson = new Gson();
		return gson.fromJson(json, clazz);
	}

	public static Step jsonToStep(String json){
		return jsonToObject(json, Step.class);
	}

}
