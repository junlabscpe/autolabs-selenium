package com.automationlabs.framework.selenium.utility;

public final class ConfigPropertyKeys {
    public static final String WEBDRIVER_CHROME = "webdriver.chrome";
	public static final String WEBDRIVER_FIREFOX = "webdriver.firefox";
	public static final String WEBDRIVER_OPERA = "webdriver.opera";
	public static final String OPERA_BINARY_LOCAL = "opera.binary.local";
	public static final String OPERA_BINARY_GRID = "opera.binary.grid";

	public static final String DEFAULT_WAIT_SEC = "default.wait.seconds";
	public static final String SUITES_LOCATION = "suites.location";
}
