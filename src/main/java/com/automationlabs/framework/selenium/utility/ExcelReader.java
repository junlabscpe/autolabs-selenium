package com.automationlabs.framework.selenium.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.automationlabs.framework.selenium.datadriven.Step;

public class ExcelReader {

	public static List<Step> getSteps(String excelFilePath, String sheet) throws IOException{
		FileInputStream fls = new FileInputStream(new File(excelFilePath));
		Workbook book = new XSSFWorkbook(fls);

		Sheet sht = book.getSheet(sheet);
		List<Step> steps = new ArrayList<Step>();

		for(int i = 0; i < sht.getPhysicalNumberOfRows(); i++){
			Row row = sht.getRow(i);
			Step step = new Step();
			step.setStepNo((int)row.getCell(0,Row.CREATE_NULL_AS_BLANK).getNumericCellValue());
			step.setKeyword(row.getCell(1,Row.CREATE_NULL_AS_BLANK).getStringCellValue());
			step.setLocatorType(row.getCell(2,Row.CREATE_NULL_AS_BLANK).getStringCellValue());
			step.setLocator(row.getCell(3,Row.CREATE_NULL_AS_BLANK).getStringCellValue());
			step.setTestData(row.getCell(4,Row.CREATE_NULL_AS_BLANK).getStringCellValue());

			steps.add(step);
		}

		book.close();
		fls.close();

		return steps;
	}

	public static void main(String[] args) throws IOException {
		List<Step> steps = getSteps("target\\classes\\scripts\\TC1.xlsx", "Sheet1");
		for(Step step: steps){
			System.out.println(step.toString());
		}
	}

}
