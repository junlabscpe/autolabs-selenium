package com.automationlabs.framework.selenium.utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.automationlabs.framework.selenium.datadriven.Step;

public class SeleniumUtils {

	public enum BrowserType{
		CHROME, FIREFOX, OPERA
	}

	public enum LocatorTypes{
		XPATH, ID, CSSSELECTOR, CLASSNAME, PARTIALLINKTEXT, LINKTEXT, NAME, TAGNAME
	}

	public static WebElement getElement(WebDriver driver, Step step){
		return driver.findElement(getLocator(step.getLocatorType(),step.getLocator()));
	}

	public static By getLocator(String locatorType, String locator){
		switch(LocatorTypes.valueOf(locatorType.toUpperCase())){
			case XPATH:
				return By.xpath(locator);
			case ID:
				return By.id(locator);
			case CSSSELECTOR:
				return By.cssSelector(locator);
			case CLASSNAME:
				return By.className(locator);
			case PARTIALLINKTEXT:
				return By.partialLinkText(locator);
			case LINKTEXT:
				return By.linkText(locator);
			case NAME:
				return By.name(locator);
			case TAGNAME:
				return By.tagName(locator);
			default:
				return null;
		}
	}

	public static void turnDefaultImplicitWaitOn(WebDriver driver){
		driver.manage().timeouts().implicitlyWait(ConfigProperties.getLongProperty(ConfigPropertyKeys.DEFAULT_WAIT_SEC), TimeUnit.SECONDS);
	}

	public static void turnDefaultImplicitWaitOff(WebDriver driver){
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}
}
