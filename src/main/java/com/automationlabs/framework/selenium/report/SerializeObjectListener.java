package com.automationlabs.framework.selenium.report;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

public class SerializeObjectListener implements IReporter {

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {

		Set<ITestResult> results = new HashSet<>();

		for(ISuite suite: suites){
			for(ISuiteResult result : suite.getResults().values() ){
				results.addAll(result.getTestContext().getPassedTests().getAllResults());
				results.addAll(result.getTestContext().getFailedTests().getAllResults());
				results.addAll(result.getTestContext().getSkippedTests().getAllResults());
			}
		}

		List<String> resultsSer = new ArrayList<>();
		for(ITestResult result: results){
			resultsSer.add(result.getName());
		}

		Result result = new Result(resultsSer);
		try {
			FileOutputStream f = new FileOutputStream(new File("C:\\jba\\workspaces\\framework-design\\target\\Result.res"));
			ObjectOutputStream o = new ObjectOutputStream(f);

			o.writeObject(result);

			o.close();
			f.close();

		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

}
