package com.automationlabs.framework.selenium.report;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;
import org.uncommons.reportng.ReportNGException;
import org.uncommons.reportng.ReportNGUtils;

import com.automationlabs.framework.selenium.datadriven.Step;
import com.automationlabs.framework.selenium.utility.UtilOthers;

public class HTMLReporterGen implements IReporter {

	private static final String ENCODING = "UTF-8";
	private static final String TEMPLATE_EXT = ".vm";
	private static final String HTML_FOLDER = "/html";
	private static final String TEMPLATES_PATH = "report/html/";
	private static final String RESOURCES_PATH = "report/resources/";
	private static final ReportNGUtils utils = new ReportNGUtils();
	private static final String REPORTNG_UTILS_KEY = "utils";
	private static final String UTIL_OTHERS_KEY = "utilOthers";

	private static final String TESTCASE_STEPS_TEMPLATE = "TestCaseSteps.html";
	private static final String TESTCASE_STEPS_KEY = "steps";

	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {

		Velocity.setProperty("resource.loader", "classpath");
		Velocity.setProperty("classpath.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		Velocity.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");

		try {
			Velocity.init( );
			createTestCase(suites, outputDirectory);
			copyClasspathResource(new File(outputDirectory + HTML_FOLDER), "bootstrap.min.css", "bootstrap.min.css");
			copyClasspathResource(new File(outputDirectory + HTML_FOLDER), "bootstrap.min.js", "bootstrap.min.js");
		} catch (Exception ex) {
			throw new ReportNGException("Failed to initialise Velocity.", ex);
		}
	}

	private void createTestCase(List<ISuite> suites, String outputDirectory) throws Exception {
		for (ISuite suite : suites) {
			for (ISuiteResult result : suite.getResults().values()) {
				Set<ITestResult> results = new HashSet<ITestResult>();
				results.addAll(result.getTestContext().getPassedTests().getAllResults());
				results.addAll(result.getTestContext().getFailedTests().getAllResults());
				results.addAll(result.getTestContext().getSkippedTests().getAllResults());

				ITestResult[] res = new ITestResult[results.size()];
				for (ITestResult re : results) {
					Step step = UtilOthers.jsonToObject(utils.getArguments(re), Step.class);
					res[step.getStepNo() - 1] = re;
				}

				VelocityContext context = new VelocityContext();
				context.put(REPORTNG_UTILS_KEY, utils);
				context.put(UTIL_OTHERS_KEY, new UtilOthers());
				context.put(TESTCASE_STEPS_KEY, Arrays.asList(res));
				generateFile(outputDirectory,
						suite.getName() + "_" + result.getTestContext().getName() + "_" + TESTCASE_STEPS_TEMPLATE,
						TESTCASE_STEPS_TEMPLATE + TEMPLATE_EXT, context, TEMPLATES_PATH);
			}
		}
	}

	protected void generateFile(String parent, String child, String templateName, VelocityContext context, String path)
			throws Exception {
		File file = new File(parent + HTML_FOLDER);
		if (!file.exists())
			file.mkdirs();
		Writer writer = new BufferedWriter(new FileWriter(new File(parent + HTML_FOLDER, child)));
		try {
			Velocity.mergeTemplate(path + templateName, ENCODING, context, writer);
			writer.flush();
		} finally {
			writer.close();
		}
	}

	protected void copyClasspathResource(File outputDirectory, String resourceName, String targetFileName)
			throws IOException {
		String resourcePath = RESOURCES_PATH + resourceName;
		InputStream resourceStream = getClass().getClassLoader().getResourceAsStream(resourcePath);
		copyStream(outputDirectory, resourceStream, targetFileName);
	}

	protected void copyStream(File outputDirectory, InputStream stream, String targetFileName) throws IOException {
		File resourceFile = new File(outputDirectory, targetFileName);
		BufferedReader reader = null;
		Writer writer = null;
		try {
			reader = new BufferedReader(new InputStreamReader(stream, ENCODING));
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resourceFile), ENCODING));

			String line = reader.readLine();
			while (line != null) {
				writer.write(line);
				writer.write('\n');
				line = reader.readLine();
			}
			writer.flush();
		} finally {
			if (reader != null) {
				reader.close();
			}
			if (writer != null) {
				writer.close();
			}
		}
	}

}
