package com.automationlabs.framework.selenium.report;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private List<String> suites;

	public Result(List<String> suites){
		this.suites = suites;
	}

	public List<String> getSuites(){
		return suites;
	}

}
