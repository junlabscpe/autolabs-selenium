package test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.automationlabs.framework.selenium.webdriver.Browser;
import com.automationlabs.framework.selenium.webdriver.ChromeBrowser;

public class DriverTest {

    Browser browser;
    
    @BeforeClass
    public void setup(){
        browser = new ChromeBrowser("C:\\Users\\Jn\\Desktop\\Temp Files\\chromedriver.exe");
    }

    @Test
    public void googleTest() throws Exception{
        SoftAssert softAssert = new SoftAssert();
        browser.navigate("https://www.google.com/");
        browser.sendKeys("//input[@aria-label='Search']", "Selenium WebDriver Test\n");
        softAssert.assertTrue(browser.isElementPresent("result-stats"));
        browser.wait(5);
    }

    @AfterClass
    public void teardown(){
        browser.close();
    }
}
