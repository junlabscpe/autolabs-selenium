package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.automationlabs.framework.selenium.report.Result;

public class ParserTest {

	@Test
	public void test1(){
		Assert.assertTrue(true);
	}

	@Test
	public void test2(){
		throw new SkipException("Test Skipped.");
	}
	@Test
	public void test3(){
		Assert.assertTrue(false);
	}
	@Test(dependsOnMethods = "test3")
	public void test4(){

	}
	@Test
	public void test5(){
		Assert.assertTrue(true);
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		FileInputStream fi = new FileInputStream(new File("C:\\jba\\workspaces\\framework-design\\target\\Result.res"));
		ObjectInputStream oi = new ObjectInputStream(fi);

		// Read objects
		Result result = (Result) oi.readObject();


		oi.close();
		fi.close();

		List<String> suites = result.getSuites();
		System.out.println(suites.toString());
	}

}
